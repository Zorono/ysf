# YSF - Nobody's version

YSF is a special addon, which purpose is to pull out maximum possibilities from the SA:MP Server, mainly with memory editing and hooking.

Visit [SA-MP Forums topic](https://forum.sa-mp.com/showthread.php?t=513499) to learn more or visit the [official GitHub repository](https://github.com/IllidanS4/YSF).

Follow [@n0bodysec](https://twitter.com/n0bodysec) on Twitter if you are cool.

**Please take a look at [Fork network](https://gitlab.com/n0bodysec/ysf/wikis/Fork-network) wiki page.**

## Documentation

Almost all natives and callbacks can be found on our [wiki](https://gitlab.com/n0bodysec/ysf/wikis/home).

## Installing

Please see [install instructions](https://gitlab.com/n0bodysec/ysf/wikis/home#instructions).

## Building

* [Linux](https://gitlab.com/n0bodysec/ysf/wikis/home#linux)
* [Windows](https://gitlab.com/n0bodysec/ysf/wikis/home#windows)

## Discussion

* Discuss YSF on the [forums](https://forum.sa-mp.com/showthread.php?t=513499).

## License

[MPL 1.1](https://www.mozilla.org/en-US/MPL/1.1/)

Planning to move to GPLv2.
